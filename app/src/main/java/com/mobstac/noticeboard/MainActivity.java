package com.mobstac.noticeboard;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mobstac.beaconstac.Beaconstac;
import com.mobstac.beaconstac.core.MSException;
import com.mobstac.beaconstac.interfaces.BeaconScannerCallbacks;
import com.mobstac.beaconstac.interfaces.BeaconstacNotification;
import com.mobstac.beaconstac.interfaces.MSErrorListener;
import com.mobstac.beaconstac.interfaces.MSSyncListener;
import com.mobstac.beaconstac.models.MBeacon;
import com.mobstac.beaconstac.models.MRule;
import com.mobstac.beaconstac.utils.MSConstants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static no.nordicsemi.android.dfu.DfuBaseService.NOTIFICATION_ID;

public class MainActivity extends AppCompatActivity implements Serializable{

    int beaconid = 0;
    Context ctx = this;
    TextView res = null;
    HashMap<Long,Integer> timeBeacon = new HashMap<Long,Integer>();
    HashMap<Long,Notification.Builder> timeNotification = new HashMap<Long,Notification.Builder>();
    Long curr_time = 0l;
    String isStudent = "FALSE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = (TextView) findViewById(R.id.textView);
        initializeAction();
        ToggleButton toggle = findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    startToggle();
                } else {
                    // The toggle is disabled
                    stopToggle();
                }
            }
        });

        Beaconstac.getInstance().setUserName("Sam" , "Daniel");
        Beaconstac.getInstance().setBeaconScannerCallbacks(new BeaconScannerCallbacks() {

            @Override
            public void onCampedBeacon(MBeacon beacon) {
                System.out.println("On Camped Beacon Triggered");
                curr_time = new Date().getTime();
                TimerTask task = new BeaconTimerTask();
                Timer timer = new Timer();
                timer.schedule(task,new Date(curr_time + 10000));
                beaconid = beacon.getId();
                res.append("Beacon ID : " + Integer.toString(beaconid) + "\n");
                timeBeacon.put(curr_time,beaconid);
            }

            @Override
            public void onScannedBeacons(ArrayList<MBeacon> rangedBeacons) {
            }

            @Override
            public void onExitedBeacon(MBeacon beacon) {
                beaconid = Integer.MAX_VALUE;
                curr_time = Long.MAX_VALUE;
            }

            @Override
            public void onRuleTriggered(MRule rule) {
            }
        });

        Beaconstac.getInstance().overrideBeaconstacNotification(new BeaconstacNotification() {
            @Override
            public void notificationTrigger(Notification.Builder notification) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Notification Overrider");
                String title = notification.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString();
                System.out.println("&" + title);

                if(title.startsWith("#Notice")) {
                    ArrayList<String> al = new ArrayList<>();
                    al.add(Integer.toString(beaconid));
                    String person = null;
                    if(isStudent.equals("TRUE"))
                        person = "stud";
                    else
                        person = "fac";
                    al.add(person);
                    Intent resultIntent = new Intent(getApplicationContext(), ResultActivity.class).putExtra("Info", al);

                    resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    ctx,
                                    (int) System.currentTimeMillis(),
                                    resultIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    Log.d("Notification", "Passes");
                    notification.setContentIntent(resultPendingIntent);
                    timeNotification.put(curr_time, notification);
                }

            }
        });

    }

    public class BeaconTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            long orig_time = scheduledExecutionTime() - 10000;
            int orig_beacon_id = timeBeacon.get(orig_time);
            if(orig_beacon_id == beaconid)
            {
                //Push the Notification
                Notification.Builder notification = timeNotification.get(orig_time);
                NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                printHashMap();
                if(notification != null)
                    mNotifyMgr.notify(NOTIFICATION_ID, notification.build());
                else
                    System.out.println("Notification null");
            }
            else
            {
                Log.d("Notification","User went out of range within 5s");
            }

        }
    }

    public void printHashMap()
    {
        for(long a:timeBeacon.keySet())
        {
            System.out.println("*" + a + ":" + timeBeacon.get(a));
        }
        for(long b:timeNotification.keySet())
        {
            System.out.println("#" + b + ":" + timeNotification.get(b));
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        Beaconstac.getInstance().stopScanningBeacons(null);
        initializeAction();
    }



    private void initializeAction() {
        if(checkPermission()) {
            try {
                Beaconstac.initialize(getApplicationContext(), "e62435a78e67ec98bba3b879ba00448650032557", new MSSyncListener() {
                    @Override
                    public void onSuccess() {
                        Log.d("Beaconstac", "Initialization successful");
                        //Beaconstac.getInstance().setFilterValue("age",20);

                    }

                    @Override
                    public void onFailure(MSException e) {
                        Log.d("Beaconstac", "Initialization failed");
                    }

                });
                Beaconstac.getInstance().setFilterValue("isStudent",isStudent);
            } catch (MSException | SecurityException e) {
                e.printStackTrace();
            }
            System.out.println("Initialized");
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MSConstants.REQUEST_LOCATION_PERMISSION);
            return false;
        } else {
            return true;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MSConstants.REQUEST_LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initializeAction();
                }
            }
        }
    }


    private void stopToggle() {
        try {
            Beaconstac.getInstance().stopScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Stopped Scanning");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void startToggle() {
        try {
            Beaconstac.getInstance().startScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Started Scanning");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
