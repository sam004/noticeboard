package com.mobstac.noticeboard;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by samdaniel on 19/01/18.
 */

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        printDetails();
    }
    public void printDetails()
    {
        Database db = new Database(getApplicationContext());

        ArrayList<String> al = (ArrayList<String>) getIntent().getSerializableExtra("Info");
        al = db.getDetails(Integer.parseInt(al.get(0)),al.get(1));
        ListView list = findViewById(R.id.listview);
        list.setAdapter(new ArrayAdapter<String>(
                this, R.layout.list_item, al));

    }
}
